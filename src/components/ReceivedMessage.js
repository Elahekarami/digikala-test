import React, { useState, useEffect } from "react"
import styles from "../styles/Message.module.scss"

export default function ReceivedMessage() {

    const [message, setMessage] = useState()

    const randomMessages = [
        "I'm so happy you sent a message",
        "It's a great day. How do you feel to go for a ride?",
        "We went to a resturant for dinner last night. It was a lovely resturant and we all enjoyed",
        "I understant, but are you sure?"
    ]

    useEffect(() => {
        let m = randomMessages[Math.floor(Math.random() * randomMessages.length)]
        setMessage(m)
        document.getElementById("message-box").scrollTop = document.getElementById("message-box").scrollHeight;
    },[])

    return (
        <div className={`${styles.message} ${styles.received}`}>
            {message}
            <span className={styles.metadata}>
                <span className={styles.time}>{new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' })}</span>
            </span>
        </div>
    )
}