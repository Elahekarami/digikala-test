import React, {useState} from 'react'
import {IoMdSend} from "react-icons/io"
import styles from "../styles/InputBox.module.scss"

export default function InputBox({message, onClickHandler, onChangeHandler, onKeyPressHandler}) {

    return(
        <div className={styles.input_box}>
            <textarea 
                className={styles.textarea}
                type="text" 
                name="message" 
                value={message} 
                placeholder='Enter your message' 
                rows="1"
                onKeyPress={e => onKeyPressHandler(e)}
                onChange={e => onChangeHandler(e)} 
            />
            <button className={styles.send_btn} onClick={onClickHandler}>
                <IoMdSend />
            </button>
        </div>
    )
}