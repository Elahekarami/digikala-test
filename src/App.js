import React from 'react';
import Header from "./containers/Header"
import ChatBox from "./containers/ChatBox"

import "./styles/global.scss"

function App() {
  return (
    <div className="App">
      <Header />
      <ChatBox />
    </div>
  );
}

export default App;
