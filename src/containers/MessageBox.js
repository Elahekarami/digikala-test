import React, { useEffect } from "react"
import SentMessage from "../components/SentMessage"
import ReceivedMessage from "../components/ReceivedMessage"

export default function MessageBox({messages}) {

    useEffect(() => {
        document.getElementById("message-box").scrollTop = document.getElementById("message-box").scrollHeight;
    }, [messages])

    return (
        <div id="message-box" style={{height: "calc(100% - 65px)", overflowY: "scroll"}}>
            <SentMessage message="Hello" />
            <ReceivedMessage />
            <SentMessage message="This is Elahe Karami" />
            <ReceivedMessage />
            {
                messages.length
                ?
                messages.map((message, index)=> <SentMessage key={index} message={message} justSent={true} />)
                :
                ""
            }
        </div>
    )
}