import React, {useState, useEffect} from 'react'
import InputBox from "../components/InputBox"
import MessageBox from "./MessageBox"
import styles from "../styles/ChatBox.module.scss"

export default function ChatBox() {

    const [message, setMessage] = useState("")
    const [messageArray, setMessageArray] = useState([])

    const onChangeHandler = (e) => {
        const {value} = e.target
        setMessage(value)
    }

    const onClickHandler = () => {
        setMessage("")
        setMessageArray([...messageArray, message])
        
    }

    const onKeyPressHandler = (e) => {
        var code = e.keyCode ? e.keyCode : e.which;
        if (code == 13) {
          e.preventDefault()
          setMessage("")
          setMessageArray([...messageArray, message])
        }
    }

    return(
        <div className={styles.chatBox}>
            <MessageBox messages={messageArray} />
            <InputBox 
                message={message} 
                onChangeHandler={onChangeHandler} 
                onClickHandler={onClickHandler} 
                onKeyPressHandler={onKeyPressHandler}
            />    
        </div>
    )
}