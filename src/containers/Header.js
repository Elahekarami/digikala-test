import React, { useState } from 'react'
import { BsThreeDotsVertical } from "react-icons/bs"
import User from "./User"
import styles from "../styles/Header.module.scss"

export default function Header() {
    const [name, setName] = useState("Elahe Karami")
    const [status, setStatus] = useState("online")
    const [number, setnumber] = useState("09120000000")
    const [bio, setBio] = useState("This is me")
    const [openMenu, setOpenMenu] = useState(false)

    const onClickHandler = () => {
        setOpenMenu(true)
    }

    const menuHandler = () => {
        setOpenMenu(false)
    }

    return(
        <div>
            <div className={styles.header}>
                <div className={styles.profile_info} onClick={onClickHandler}>
                    <div className={styles.profile_img}>
                        <img src={require(`../assets/images/profile.jpg`)} />
                    </div>
                    <div className={styles.profile_details}>
                        <p className={styles.name}>{name}</p>
                        <p className={styles.time}>{status}</p>
                    </div>
                </div>
                <div className={styles.settings}>
                    <BsThreeDotsVertical />
                </div>
            </div>
            <User open={openMenu} user={{name, status, number, bio}} menuHandler={menuHandler} />
        </div>
    )
}