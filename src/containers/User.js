import React, { useState } from "react"
import {ImArrowLeft2} from "react-icons/im"
import styles from "../styles/User.module.scss"

export default function User({open, user, menuHandler}){

    return (
        <div className={`${styles.user} ${open && styles.open}`}>
            <div className={styles.back} onClick={menuHandler}><ImArrowLeft2 /></div>
            <div className={styles.profile_info}>
                <div className={styles.profile_img}>
                    <img src={require('../assets/images/profile.jpg')} />
                    <div className={styles.profile_img_text}>
                        <h2 className={styles.name}>{user.name}</h2>
                        <p>{user.status}</p>
                    </div>
                    <div className={styles.overlay}></div>
                </div>
                <div className={styles.profile_details}>
                    <p className={styles.info_title}>Info</p>
                    <div className={styles.info_box}>
                        <p className={styles.info}>{user.number}</p>
                        <p className={styles.label}>Mobile</p>
                    </div>
                    <div className={styles.info_box}>
                        <p className={styles.info}>{user.bio}</p>
                        <p className={styles.label}>Bio</p>
                    </div>
                    <div className={styles.info_box}>
                        <p className={styles.info}>@{user.name}</p>
                        <p className={styles.label}>Username</p>
                    </div>
                </div>
            </div>
        </div>
    )
}